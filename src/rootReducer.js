import { combineReducers } from 'redux';

import user from './Logon/reducer';

const rootReducer = combineReducers({
  user,
});

export default (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    return rootReducer(undefined, action);
  }

  return rootReducer(state, action);
};
