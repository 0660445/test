import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Root from './Root';

function renderApp(App) {
  ReactDOM.render(<App />, document.getElementById('root'));
}

renderApp(Root);
