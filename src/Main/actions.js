import { createAction } from 'redux-actions';

export const logout = createAction('USER_LOGOUT');
export const initApp = createAction('INIT_APP');
