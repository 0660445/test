import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import history from '../history';
import { logout as logoutAction } from './actions';
import { setUser as setUserAction } from '../Logon/actions';
import { API_URL } from '../constants/api';

function* logout() {
  try {
    yield axios.post(`${API_URL}/users/logout`);
    yield put(setUserAction(''));
    yield call(history.push, '/logon');
  } catch (e) {}
}

export function* initApp() {
  try {
    const result = yield axios.get(`${API_URL}/users/current`);
    yield put(setUserAction(result.data.login));
    // yield call(history.push, '/');
  } catch (e) {
    yield put(setUserAction(''));
  }
}

export default function*() {
  yield all([
    takeLatest('INIT_APP', initApp),
    takeLatest(logoutAction, logout),
  ]);
}
