import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export class Main extends React.PureComponent {
  static propTypes = {
    userName: PropTypes.string.isRequired,
  };

  render() {
    const { userName } = this.props;

    return <h1 className="AppHeader">Welcome, {userName}</h1>;
  }
}

export const mapStateToProps = state => ({
  userName: state.user.userName,
});

export default connect(mapStateToProps)(Main);
