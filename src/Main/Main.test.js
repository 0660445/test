import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import TestRenderer from 'react-test-renderer';
import { Main, mapStateToProps } from './Main';
// import ShallowRenderer from 'react-test-renderer/shallow';

describe('render test', () => {
  const renderer = TestRenderer.create(
    <Main userName="test" logout={jest.fn()} />
  );

  it('snapshot', () => {
    const tree = renderer.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly', () => {
    const result = renderer.root;

    expect(result.findByProps({ className: 'AppHeader' }).children).toEqual([
      'Welcome, ',
      'test',
    ]);
  });
});

describe('connect test', () => {
  it('test mapStateToProps', () => {
    const initialState = { user: { userName: 'test' } };
    expect(mapStateToProps(initialState).userName).toEqual('test');
  });
});
