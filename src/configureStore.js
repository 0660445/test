import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

export default () => {
  const composeEnhancers = composeWithDevTools({
    // actionsBlacklist: ['@@redux-form.*'],
  });
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  );

  sagaMiddleware.run(rootSaga);

  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      store.replaceReducer(require('./rootReducer').default);
    });

    module.hot.accept('./rootSaga', () => {
      sagaMiddleware.cancelSagas(store);
      require('./rootSaga').default.startSagas(sagaMiddleware);
    });
  }

  return store;
};
