import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import PrivateRoute from './PrivateRoute/index';
import Layout from '../App/index';
import Logon from '../Logon/index';
import history from '../history';
import NotFound from '../NotFound/index';

export default () => (
  <Router history={history}>
    <Switch>
      <Route path="/logon" component={Logon} />
      <PrivateRoute component={Layout} />
      <Route component={NotFound} />
    </Switch>
  </Router>
);
