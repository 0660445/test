import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import PrivateRoute from './PrivateRoute/index';
import App from '../Main/index';
import history from '../history';
import NotFound from '../NotFound/index';

const ROUTES = [
  {
    path: '/',
    component: App,
    exact: true,
  },
];

export default props => (
  <Router history={history}>
    <Switch>
      {ROUTES.map((route, i) => <PrivateRoute key={i} {...props} {...route} />)}
      <Route component={NotFound} />
    </Switch>
  </Router>
);
