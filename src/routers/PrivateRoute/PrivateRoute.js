import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({
  component: Component,
  path,
  userInfoReceived,
  userName,
}) => {
  if (!userInfoReceived) {
    return <div className="Loading">Loading...</div>;
  }
  return (
    <Route
      path={path}
      render={props =>
        userName ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/logon',
            }}
          />
        )
      }
    />
  );
};

export default connect(state => ({
  userName: state.user.userName,
  userInfoReceived: state.user.userInfoReceived,
}))(PrivateRoute);
