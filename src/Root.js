import React from 'react';
import { Provider } from 'react-redux';
import { hot } from 'react-hot-loader';
import Router from './routers/rootRouter';
import configureStore from './configureStore';
import { initApp } from './Main/actions';

const store = configureStore();

const Root = () => (
  <Provider store={store} key={Math.random()}>
    <Router />
  </Provider>
);

store.dispatch(initApp());

export default hot(module)(Root);
