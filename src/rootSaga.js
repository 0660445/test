import { all, fork } from 'redux-saga/effects';

import appSagas from './Main/sagas';
import logonSagas from './Logon/sagas';

export default function* root() {
  yield all([fork(appSagas)]);
  yield all([fork(logonSagas)]);
}
