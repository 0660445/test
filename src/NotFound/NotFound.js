import React from 'react';
import { Link } from 'react-router-dom';

export default function NotFound() {
  return (
    <div className="NotFound">
      <Link to="/">Page not found</Link>
    </div>
  );
}
