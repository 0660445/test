import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Router from '../routers/privateRouter';
import { logout } from '../Main/actions';

export class App extends React.PureComponent {
  static propTypes = {
    logout: PropTypes.func.isRequired,
  };

  render() {
    const { logout } = this.props;

    return (
      <div className="Layout">
        <button className="Btn BtnLogout" onClick={logout}>
          Logout
        </button>
        <Router />
      </div>
    );
  }
}

export const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(
  null,
  mapDispatchToProps
)(App);
