import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import { App, mapDispatchToProps } from './App';

describe('render test', () => {
  const renderer = new ShallowRenderer();
  renderer.render(<App logout={jest.fn()} />);
  const result = renderer.getRenderOutput();

  it('snapshot', () => {
    expect(result).toMatchSnapshot();
  });

  it('renders correctly', () => {
    expect(result.type).toBe('div');
  });
});

describe('connect test', () => {
  it('test mapDispatchToProps', () => {
    const dispatch = jest.fn();

    mapDispatchToProps(dispatch).logout();
    expect(dispatch.mock.calls[0][0]).toEqual({ type: 'USER_LOGOUT' });
  });
});
