import { all, call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import * as actions from './actions';
import history from '../history';
import { API_URL } from '../constants/api';

function* logon({ payload }) {
  try {
    const result = yield axios.post(`${API_URL}/users/logon`, payload);
    yield put(actions.setUser(result.data.login));
    yield call(history.push, '/');
  } catch (e) {}
}

export default function*() {
  yield all([takeLatest(actions.logon, logon)]);
}
