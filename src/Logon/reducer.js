import { handleActions } from 'redux-actions';
import * as actions from './actions';

const defaultState = {
  userName: '',
  userInfoReceived: false,
};

export default handleActions(
  {
    [actions.setUser]: (state, { payload }) => ({
      ...state,
      userName: payload,
      userInfoReceived: true,
    }),
  },
  defaultState
);
