import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logon } from './actions';

class Logon extends React.PureComponent {
  static propTypes = {
    logon: PropTypes.func.isRequired,
  };

  constructor() {
    super();
    this.state = {
      login: '',
      password: '',
    };
  }

  handleChange = type => e => {
    this.setState({
      [type]: e.target.value,
    });
  };

  handleSubmit = () => {
    const { logon } = this.props;
    logon({ ...this.state });
  };

  render() {
    const { login, password } = this.state;

    return (
      <div className="Layout">
        <div className="Form">
          <input
            autoComplete="off"
            className="Input"
            name="login"
            placeholder="Login"
            onChange={this.handleChange('login')}
            type="text"
            value={login}
          />
          <input
            autoComplete="off"
            className="Input"
            name="password"
            placeholder="Password"
            onChange={this.handleChange('password')}
            type="password"
            value={password}
          />
          <button
            className="Btn BtnLogon"
            type="button"
            onClick={this.handleSubmit}
            disabled={!login || !password}
          >
            Logon
          </button>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { logon }
)(Logon);
